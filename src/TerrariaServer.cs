﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using Terraria;
using Terraria.IO;
using Terraria.Localization;
using Terraria.Social;
using uMod.Common;
using uMod.Text;

namespace uMod.Game.Terraria
{
    /// <summary>
    /// Represents the server hosting the game instance
    /// </summary>
    public class TerrariaServer : IServer
    {
        /// <summary>
        /// Gets or sets the player manager
        /// </summary>
        public IPlayerManager PlayerManager { get; set; }

        #region Server Information

        /// <summary>
        /// Gets/sets the public-facing name of the server
        /// </summary>
        public string Name
        {
            get => Main.worldName;
            set => Main.worldName = value;
        }

        private static IPAddress address;
        private static IPAddress localAddress;

        /// <summary>
        /// Gets the public-facing IP address of the server, if known
        /// </summary>
        public IPAddress Address
        {
            get
            {
                try
                {
                    if (address == null)
                    {
                        string serverIp = Netplay.ServerIP.ToString(); // TODO: Unnecessary ToString() and TryParse below
                        if (Utility.ValidateIPv4(serverIp) && !Utility.IsLocalIP(serverIp))
                        {
                            IPAddress.TryParse(serverIp, out address);
                            Interface.uMod.LogInfo($"IP address from command-line: {address}"); // TODO: Localization
                        }
                        else
                        {
                            WebClient webClient = new WebClient();
                            IPAddress.TryParse(webClient.DownloadString("http://api.ipify.org"), out address);
                            Interface.uMod.LogInfo($"IP address from external API: {address}"); // TODO: Localization
                        }
                    }

                    return address;
                }
                catch (Exception ex)
                {
                    Interface.uMod.LogWarning("Couldn't get server's public IP address", ex); // TODO: Localization
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the local IP address of the server, if known
        /// </summary>
        public IPAddress LocalAddress
        {
            get
            {
                try
                {
                    return localAddress ?? (localAddress = Utility.GetLocalIP());
                }
                catch (Exception ex)
                {
                    Interface.uMod.LogWarning("Couldn't get server's local IP address", ex); // TODO: Localization
                    return IPAddress.Any;
                }
            }
        }

        /// <summary>
        /// Gets the public-facing network port of the server, if known
        /// </summary>
        public ushort Port => (ushort)Netplay.ListenPort;

        /// <summary>
        /// Gets the version or build number of the server
        /// </summary>
        public string Version => Main.versionNumber;

        /// <summary>
        /// Gets the network protocol version of the server
        /// </summary>
        public string Protocol => Main.curRelease.ToString();

        /// <summary>
        /// Gets the language set by the server
        /// </summary>
        public CultureInfo Language => CultureInfo.InstalledUICulture;

        /// <summary>
        /// Gets the total of players currently on the server
        /// </summary>
        public int Players => Main.PlayerList.Count;

        /// <summary>
        /// Gets/sets the maximum players allowed on the server
        /// </summary>
        public int MaxPlayers
        {
            get => Main.maxNetPlayers;
            set => Main.maxNetPlayers = value;
        }

        /// <summary>
        /// Gets/sets the current in-game time on the server
        /// </summary>
        public DateTime Time
        {
            get => new DateTime((long)Main.time);
            set => Main.time = value.Second;
        }

        /// <summary>
        /// Gets the current or average frame rate of the server
        /// </summary>
        public int FrameRate
        {
            get => Main.frameRate; // TODO: Verify this is correct
        }

        /// <summary>
        /// Gets/sets the target frame rate for the server
        /// </summary>
        public int TargetFrameRate
        {
            get => Main.frameRate; // TODO: This is not the target frame rate
            set => Main.frameRate = value;
        }

        /// <summary>
        /// Gets information on the currently loaded save file
        /// </summary>
        ISaveInfo IServer.SaveInfo => throw new NotImplementedException(); // TODO: Implement when possible

        #endregion Server Information

        #region Server Administration

        /// <summary>
        /// Saves the server and any related information
        /// </summary>
        public void Save()
        {
            Main.SaveSettings();
            WorldFile.SaveWorld();
        }

        /// <summary>
        /// Shuts down the server, with optional saving and delay
        /// </summary>
        public void Shutdown(bool save = true, int delay = 0)
        {
            if (save)
            {
                Save(); // TODO: Check to make sure this is not already done on quit
            }

            // TODO: Implement optional delay
            Netplay.Disconnect = true;
            SocialAPI.Shutdown();
            Process.GetCurrentProcess().Kill(); // TODO: Might not be necessary
        }

        #endregion Server Administration

        #region Player Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string playerId, string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned(playerId))
            {
                // Ban player
                int.TryParse(playerId, out int clientId);
                Netplay.AddBan(clientId);

                // Kick player with reason
                Kick(playerId, reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        /// <param name="playerId"></param>
        public TimeSpan BanTimeRemaining(string playerId) => IsBanned(playerId) ? TimeSpan.MaxValue : TimeSpan.Zero;

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        /// <param name="playerId"></param>
        public bool IsBanned(string playerId)
        {
            int.TryParse(playerId, out int clientId);
            RemoteClient client = Netplay.Clients[clientId];
            return client != null && Netplay.IsBanned(client.Socket.GetRemoteAddress());
        }

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        /// <param name="playerId"></param>
        public bool IsConnected(string playerId)
        {
            int.TryParse(playerId, out int clientId);
            RemoteClient client = Netplay.Clients[clientId];
            return client != null && client.Socket.IsConnected();
        }

        /// <summary>
        /// Kicks the player for the specified reason
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="reason"></param>
        public void Kick(string playerId, string reason = "")
        {
            if (IsConnected(playerId))
            {
                int.TryParse(playerId, out int clientId);
                NetMessage.SendData(2, clientId, -1, NetworkText.FromLiteral(reason));
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        /// <param name="playerId"></param>
        public void Unban(string playerId)
        {
            // Check if already unbanned
            if (IsBanned(playerId))
            {
                // Unban player
                if (File.Exists(Netplay.BanFilePath))
                {
                    int.TryParse(playerId, out int clientId);
                    string identifier = Netplay.Clients[clientId].Socket.GetRemoteAddress().GetIdentifier();
                    using (StreamWriter writer = new StreamWriter(Netplay.BanFilePath))
                    {
                        foreach (string line in File.ReadAllLines(Netplay.BanFilePath))
                        {
                            if (!line.Contains($"//{Main.player[clientId].name}") && !line.Contains(identifier))
                            {
                                writer.WriteLine(line);
                            }
                        }
                    }
                }
            }
        }

        #endregion Player Administration

        #region Chat and Commands

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message))
            {
                message = args.Length > 0 ? string.Format(Formatter.ToTerraria(message), args) : Formatter.ToTerraria(message);
                NetMessage.SendData(25, -1, -1, NetworkText.FromLiteral(prefix != null ? $"{prefix} {message}" : message), 255, 255, 0, 160);
            }
        }

        /// <summary>
        /// Broadcasts the specified chat message to all players
        /// </summary>
        /// <param name="message"></param>
        public void Broadcast(string message) => Broadcast(message, null);

        /// <summary>
        /// Runs the specified server command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Chat and Commands
    }
}
