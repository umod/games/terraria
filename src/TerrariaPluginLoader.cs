﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.Terraria
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class TerrariaPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(Terraria) };

        public TerrariaPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
