using uMod.Auth;
using uMod.Common;

namespace uMod.Game.Terraria
{
    /// <summary>
    /// Represents a Terraria player manager
    /// </summary>
    public class TerrariaPlayerManager : PlayerManager<TerrariaPlayer>
    {
        /// <summary>
        /// Create a new instance of the ReignOfKingsPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public TerrariaPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, TerrariaPlayer player)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, player))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determine if specified key matches the specified player
        /// </summary>
        /// <param name="partialNameOrIdOrIp"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        protected override bool IsPlayerKeyMatch(string partialNameOrIdOrIp, IPlayer player)
        {
            if (base.IsPlayerKeyMatch(partialNameOrIdOrIp, player))
            {
                return true;
            }

            return false;
        }
    }
}
