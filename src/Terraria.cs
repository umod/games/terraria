﻿using uMod.Plugins;
using uMod.Plugins.Decorators;

namespace uMod.Game.Terraria
{
    /// <summary>
    /// The core Terraria plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class Terraria : Plugin
    {
        #region Initialization

        internal static readonly TerrariaProvider Universal = TerrariaProvider.Instance;

        /// <summary>
        /// Initializes a new instance of the Terraria class
        /// </summary>
        public Terraria()
        {
            // Set plugin info attributes
            Title = "Terraria";
            Author = TerrariaExtension.AssemblyAuthors;
            Version = TerrariaExtension.AssemblyVersion;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();
        }

        #endregion Initialization
    }
}
